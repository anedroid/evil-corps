To moja stale aktualizowana lista artykułów na temat przewinień wielkich korporacji, jak Google, Facebook (aka Meta), Microsoft, Apple czy Amazon (ale nie tylko). Artykuły są kopiowane (przepisywane) z oryginalnych stron na wypadek, gdyby zostały usunięte, czy to przez twórcę, czy któregoś z pośredników – aby były dostępne w jeszcze jedynym miejscu, wolnym od śledzących skryptów i cookies.

## Google

- 2022.05.17 [Total Commander forced to stop letting you install APKs](google/total-commander-forced-to-stop-letting-you-install-apks.md) | [tłumaczenie](google/pl/total-commander-forced-to-stop-letting-you-install-apks.md)
- 2022.05.07 [Your Google Account has been disabled because of a python code!](google/your-google-account-has-been-disabled-because-of-a-python-code.md) | [tłumaczenie](google/pl/your-google-account-has-been-disabled-because-of-a-python-code.md)
- 2022.05.02 [Dark Pattern: How Youtube Makes Sure You Don’t Always “Skip Ad”](google/dark-pattern-how-youtube-makes-sure-you-don_t-always-skip-ad.md)
- 2018.05.21 [Google już nawet nie udaje. Słynne „don't be evil” zepchnięte na margines](google/google-dont-be-evil.md)

## Apple

- 2022.05.17 [Your iPhone Is Vulnerable to a Malware Attack Even When It’s Off](apple/your-iphone-is-vulnerable-to-a-malware-attack-even-when-it_s-off.md)

## Microsoft

- **2022.06.27 [“Magic links” can end up in Bing search results — rendering them useless.](microsoft/magic-links-can-end-up-in-bing-search-results-rendering-them-useless.md)** | [tłumaczenie](microsoft/pl/magic-links-can-end-up-in-bing-search-results-rendering-them-useless.md)

## Facebook

- 2022.06.11 [Tell HN: My wife was banned from WhatsApp without reason or recourse](facebook/tell-hn-my-wife-was-banned-from-whatsApp-without-reason-or-recourse.md) | [tłumaczenie](facebook/pl/tell-hn-my-wife-was-banned-from-whatsApp-without-reason-or-recourse.md)

---

* Mastodon: <https://mstdn.social/@anedroid>
* GnuPG: [0243 7935 DF5A A8A9 DF40 C890 F149 EE15 E69C 7F45](https://keys.openpgp.org/vks/v1/by-fingerprint/02437935DF5AA8A9DF40C890F149EE15E69C7F45)
* ultimate goal: **destroy Google**
