## "Magiczne linki" mogą się znaleźć w wynikach wyszukiwania Bing - czyniąc je bezużytecznymi

[Oryginalny artykuł](../magic-links-can-end-up-in-bing-search-results-rendering-them-useless.md) został napisany w języku angielskim.

źródło: <https://scribe.rip/magic-links-can-end-up-in-bing-search-results-rendering-them-useless-37def0fae994> (oryginalny post został opublikowany w serwisie Medium. Scribe pozwala na czytanie postów z serwisu Medium bez narażania prywatności użytkowników)

użytkownik: *Ryan Badger*

opublikowano: 2022.06.27

tłumaczenie: *Anedroid* | Jeżeli znalazłeś błąd, daj znać na Mastodonie lub wyślij pull  request.

---

![](../images/magic-links-can-end-up-in-bing-search-results-rendering-them-useless.png)

Zacząłem ostatnio weryfikować adresy email użytkowników podczas rejestracji aby zatrzymać boty, uniknąć literówek, a także upewnić się czy podane adresy są aktywne i będą mogły odbierać wiadomości oraz newslettery.

Jako że jestem leniwym programistą, szybko zaimplementowałem prosty system tokenów działających w następujący sposób:

1. Użytkownik rejestruje się z użyciem adresu email (musi również rozwiązać Google Recaptcha),
2. Generowany jest unikalny token i wysyłany na podany email jako link, który użytkownik musi kliknąć aby zweryfikować email, co automatycznie go loguje,
3. Token zostaje unieważniony (czego zapomniałem zrobić).

Ta prosta metoda weryfikacji adresu email jest często określana jako "Magic Links" (magiczne linki) - sposób na zalogowanie się do swojego konta bez konieczności podawania hasła. Założenie jest takie, że jeśli posiadasz dostęp do swojego konta email, *prawdopodobnie* jesteś tym, za kogo się podajesz.

Wszystko było ok - przynajmniej przez kilka tygodni. Później nagle zaobserwowałem wzrost liczby zalogowań, ale poza tym po zalogowaniu niewiele albo nic się nie działo na kontach użytkowników.

Po głębszej analizie zauważyłem, że wszystkie te fantomowe logowania pochodziły z **Bingbot**. W pierwszej chwili stwierdziłem, że jest to jakaś złośliwa aktywność ze sfałszowanym User Agentem, jednakże adresy IP odpowiadały adresom Bing - czyli wszystko się zgadzało.

### Ale w jaki sposób Bing logował się do kont użytkowników?

Nieco więcej logów serwera i analizy wskazało na tokeny. Wszystkie te sesje Bingbot zaczynały się od wejścia na adres URL weryfikacji z unikalnym tokenem. Nagłówka Referrer nie było.

**Jedyne** logiczne wyjaśnienie to takie, że Microsoft udostępniał treść emaili (z linkami) Bingowi w celu zindeksowania. Wszyscy wiemy, że te ESP zbierają nasze dane, ale z pewnością nie indeksują zawartości prywatnych emaili... racja?

Później szybko wyszukałem w Google (ponieważ nikt nie używa Bing) i znalazłem to:

<https://stackoverflow.com/questions/61818791/are-urls-in-emails-indexed-by-search-engines-so-they-become-publicly-searchable> ![google](../../google/icon.png)

> Od lutego 2017 Outlook (<https://outlook.live.com> ![microsoft](../../microsoft/icon.png))skanuje emaile z twojej skrzynki odbiorczej i wysyła wszystkie znalezione adresy URL do Bing w celu zindeksowania.
>
> To sprawiło, że wszelkie jednorazowe linki typu logowanie/resetowanie hasła/etc stały się bezużyteczne.

To uczucie, kiedy napotykasz na konspirację na poziomie Wikileaks... Microsoft udostępnia treść prywatnych emaili swojej wyszukiwarce internetowej?

Musiałem się upewnić... i moje obawy się potwierdziły.

### Bing indeksował moje linki do weryfikacji adresów email.

Bingbot automatycznie odwiedzał te linki, i automatycznie logował się do nowych kont użytkowników.

Na szczęście po zalogowaniu nie robił niczego (nie licząc paru kliknięć), poza tym wszystkie te konta były nowe, więc w zasadzie nie zawierały jeszcze żadnych wrażliwych danych.

## Naprawa

Aby szybko rozwiązać problem, dodałem brakujące wygaśnięcie tokenów (wszystkie tokeny teraz wygasają natychmiast po użyciu, i działają tylko przez 1h).

Ale prawdopodobnie zastąpię je formą "oto twój jednorazowy kod" (bez linków), który użytkownik musi ręcznie skopiować-wkleić na stronie, dla dodatkowego spokoju ducha.

Bez wątpienia istnieje niezliczona ilość innych (lepszych) sposobów na zabezpieczenie tego przepływu. Wykrywanie botów jest dziś dość łatwym zadaniem, więc *mógłbym* dodać dodatkowe sprawdzenie... tylko, że teraz gdy już wiem, że jakimkolwiek linkom w adresach email grozi pojawienie się w wynikach wyszukiwania Bing, jeżeli nie jest to skonfigurowane poprawnie, wolę nie używać "magicznych linków" w ogóle.
