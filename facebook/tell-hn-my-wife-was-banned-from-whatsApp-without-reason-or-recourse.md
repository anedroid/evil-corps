# Tell HN: My wife was banned from WhatsApp without reason or recourse

[Tłumaczenie](pl/tell-hn-my-wife-was-banned-from-whatsApp-without-reason-or-recourse.md)

źródło: <https://news.ycombinator.com/item?id=31707349>

użytkownik: *tempestn*

opublikowano: 2022.06.11

---

Like many people, my wife and I use WhatsApp to keep in touch with various family and friends who prefer that platform. I actually use it more than she does—she basically just chats with close family and a few friends from highschool.

A few days ago, she opened the app to find the message,

> This account is not allowed to use WhatsApp > Chats are still on this device.

This didn't make any sense, as she only uses it to chat with family and friends and certainly hadn't broken any terms. Found the option to appeal and submitted that. A day later, the app now says

> This account cannot use WhatsApp > We've completed our review and found this account's activity goes against WhatsApp's terms of service

There's no way she's actually broken any terms, so it appears this is some kind of mistake, but given the speed of the "review", I doubt any human has actually looked at it. She's tried contacting them via their contact page, but given the review is already "complete", I'm not too hopeful.

Our only guess as to what could have caused this is that a week or so before the ban she received a spam message sent to a giant group and marked it as spam. Perhaps somehow all numbers in that group ended up associated with the spamming? Regardless, pretty frustrating situation since she has a couple of long-running chats with family and friend groups that she can no longer participate in. Always fun to be at the whims of the algorithm.

Posting in case there's anything to the connection between marking a message as spam and getting blocked (maybe it's safer to just mute spam messages instead), as well as on the off chance someone from Facebook ends up reading this and can help.
