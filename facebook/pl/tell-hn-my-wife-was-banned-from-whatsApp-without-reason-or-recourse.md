# Tell HN: Moja żona została zbanowana na WhatsApp bez powodu, bez możliwości odwołania

[Oryginalny artykuł](../tell-hn-my-wife-was-banned-from-whatsApp-without-reason-or-recourse.md) został napisany w języku angielskim.

źródło: <https://news.ycombinator.com/item?id=31707349>

użytkownik: *tempestn*

opublikowano: 2022.06.11

tłumaczenie: *Anedroid* | Jeżeli znalazłeś błąd, daj znać na Mastodonie lub wyślij pull request.

---

Jak wielu ludzi, moja żona i ja korzystamy z aplikacji WhatsApp do kontaktowania się z członkami rodziny i znajomymi którzy preferują tę platformę. Właściwie to ja używam jej częściej – ona rozmawia przez WhatsApp tylko z najbliższą rodziną i kilkoma znajomymi, których poznała w liceum.

Kilka dni temu otworzyła aplikację i zobaczyła komunikat:

> Nie można korzystać z WhatsApp na tym koncie > historia czatów pozostanie na tym urządzeniu.

To nie miało żadnego sensu – skoro używa go tylko do pisania z rodziną i znajomymi – na pewno nie złamała regulaminu. Znaleźliśmy opcję odwołania się od podjętej decyzji i wysłaliśmy zgłoszenie. Następnego dnia aplikacja wyświetliła kolejny komunikat:

> Nie można korzystać z WhatsApp na tym koncie > sprawdziliśmy twoje zgłoszenie i potwierdzamy, że aktywność na tym koncie łamie warunki korzystania z usługi.

To niemożliwe aby złamała jakiekolwiek warunki, to po prostu wyglądało jak błąd. Ale biorąc pod uwagę, jak szybko zgłoszenie zostało "sprawdzone", wątpię aby jakikolwiek człowiek brał w tym udział. Próbowała skontaktować się z supportem za pośrednictwem ich strony kontaktowej, ale ponieważ zgłoszenie już zostało "sprawdzone", nie mam wielkiej nadziei, że cokolwiek się zmieni w tej sprawie.

Jedyne co przychodzi nam do głowy, co mogło spowodować tą blokadę, to że mniej-więcej tydzień wcześniej otrzymała spamową wiadomość wysłaną do ogromnej liczby użytkowników i oznaczyła to jako spam. Możliwe, że w jakiś sposób system uznał wszystkich w tej grupie za spamerów? Mimo wszystko, jest to bardzo frustrująca sytuacja. Moja żona prowadziła od dawna kilka długich czatów z rodziną i grupą przyjaciół, i teraz nie może w nich uczestniczyć. To zabawne być na łasce kaprysu algorytmu.

Wstawiam to tutaj na wypadek, gdyby istniało jakieś powiązanie między oznaczeniem wiadomości jako spam i zostaniem zbanowanym (może bezpieczniej byłoby tylko wyciszać spamowe wiadomości), oraz gdyby ktoś z Facebooka przeczytał to i mógł pomóc.
