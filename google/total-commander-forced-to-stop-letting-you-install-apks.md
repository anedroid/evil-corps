## Total Commander forced to stop letting you install APKs

**The dev took action following a Play Store policy complaint**

[Tłumaczenie](pl/total-commander-forced-to-stop-letting-you-install-apks.md)

źródło: <https://androidpolice.com/total-commander-apk-installation-block> ![google](../google/icon.png)

użytkownik: *Will Sattelberg*

opublikowano: 2022.05.17

---

One of the handiest features on Android that sets it apart from the mobile competition is the ability to install apps from outside the Play Store. APK installation is why you can still play Fortnite — even as Epic's legal battle with Google continues — and it's how you can skip the wait for automatic updates to bring the latest features to your favorite apps. Unfortunately, one of Android's most trusted file browsers has removed the ability to install APK files after receiving takedown warnings from Google.

Total Commander has been around since the 90s, eventually expanding into Android after the platform launched over a decade ago. The app has more than 10 million downloads on the Play Store, still supporting OS versions as far back as Android 2.2. With a new update, developer Christian Ghisler has removed the ability to install APK files on Android, blaming Google Play policies in the patch notes for the app. It's a shocking twist for the service and, seemingly, a bad omen of things to come for other mobile file managers.

A [forum post](https://www.ghisler.ch/board/viewtopic.php?t=76643) from Ghisler sheds some more light on what's going on here, as Google sent him a notice warning of his app's removal from the Play Store within a week if the app went unmodified. The company's automated response pointed the developer to the ["Device and Network Abuse" policy](https://support.google.com/googleplay/android-developer/answer/9888379?hl=en#zippy=%2Cexamples-of-common-violations) ![google](../google/icon.png) — specifically, these two sections:

> An app distributed via Google Play may not modify, replace, or update itself using any method other than Google Play's update mechanism. Likewise, an app may not download executable code (e.g., dex, JAR, .so files) from a source other than Google Play. This restriction does not apply to code that runs in a virtual machine or an interpreter where either provides indirect access to Android APIs (such as JavaScript in a webview or browser). 
>
> Apps or third-party code (e.g., SDKs) with interpreted languages (JavaScript, Python, Lua, etc.) loaded at run time (e.g., not packaged with the app) must not allow potential violations of Google Play policies.

Based on these rules, the Play Store's moderation system might believe Total Commander is attempting to update itself, thereby passing Google Play's update service altogether. Ghisler says he did attempt to block Total Commander's own APK from installing when you click on it, but automated systems checking his app for potential abuse didn't catch the change. He resubmitted, only to receive this vague message in return:

> As mentioned previously, your app (APK versions 1031, 1032, 1033, 1034, 1035 and 1036) causes users to download or install applications from unknown sources outside of Google Play.

According to Ghisler, he then made the decision to remove APK installations altogether, for fear of losing access to his account after a third warning — as has happened to other developers in a similar situation. 

It's possible that this block could have wide-reaching ramifications on file and web browsers in the Play Store, though the language used — not to mention [Google's poor reputation on false takedowns](https://androidpolice.com/play-store-leafsnap-simple-keyboard-takedown) ![google](../google/icon.png) — seems to hint at something less insidious. Based on the information provided by Ghisler, it seems like Google either thinks Total Commander is updating itself from within, is accidentally linking to specific APK-hosting websites, or is using a custom app installation process before navigating the user to Android's default installer. Either way, this sounds like a situation that needs some clarification from the company. Google should either spell out exactly what Total Commander is doing wrong that other file browsers have avoided, or should allow the app back on the Play Store in its previous state.

We've reached out for comment from Google and will update when we hear back. For now, you might want to block Total Commander updates if you rely on the app for routine APK installs.

![Google Play logo](images/play-store-header.png)
