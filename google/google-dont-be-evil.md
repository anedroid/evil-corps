## Google już nawet nie udaje. Słynne „don't be evil” zepchnięte na margines

źródło: <https://spidersweb.pl/2018/05/google-dont-be-evil-nie-badz-zly> ![google](../google/icon.png) ![amazon](../amazon/icon.png)

użytkownik: *Maciej Gajewski*

opublikowano: 2018.05.21

---

Wiele, wiele lat temu, kiedy Google był młodą i ambitną firmą, jako cel postawił sobie „nie być złym”. Przez kolejne lata, gdy ta rosła w siłę, kwestionowano jej przywiązanie do tego motta. Teraz Google sam je porzuca.

![](images/google-dont-be-evil.png)

Wielka władza to również wielka odpowiedzialność – ten truizm wielokrotnie jest powtarzany przez filmowych kaznodziejów. Google z pewnością można zaliczyć do podmiotów o wielkiej władzy. To firma, która zdominowała Internet na wiele różnych sposób. Zarówno jeśli chodzi o względy techniczne, jak i merytoryczne. Trzeba przyznać, że zazwyczaj za sprawą świetnie działających produktów i usług.

Google zrozumiał już dawno temu, że jej ogromny sukces będzie rodził instynktowną nieufność i obawy. Takie Mapy Google czy Wyszukiwarka Google nie tylko nie mają realnej konkurencji – funkcjonalnie bądź rynkowo – a wręcz są jednymi z najważniejszych usług w całym Internecie. Jedna firma kontrolująca przepływ informacji? Jak tu jej zaufać? Firma postanowiła więc umieścić chęć zmiany światy na lepsze w swoich fundamentach. Hasło *nie bądź zły* (*don’t be evil*) od 2000 r. stało się tożsame z marką Google.

Przez kolejne lata Google, jak prawie każda wielka korporacja, popełnił wiele… ujmijmy to dyplomatycznie, błędów. Obserwatorzy rynku zaczęli kwestionować korporacyjne motto, które – zdaniem niektórych – było używane głównie dla wygody zamiast do etycznej samokontroli. Google w końcu zdecydował się zamknąć usta krytykantom. Choć w zupełnie inny sposób, niż byśmy się spodziewali.

### „Don’t be evil” usunięte z pierwszych słów kodeksu postępowania Google.

I to nie przed chwilą, choć dopiero teraz zmianę [wypatrzyła redakcja Gizmodo](https://gizmodo.com/google-removes-nearly-all-mentions-of-dont-be-evil-from-1826153393) ![google](../google/icon.png) ![amazon](../amazon/icon.png). Według jej analizy archiwalnych wersji witryn Google’a, hasło don’t be evil zniknęło w [dostępnym publicznie](https://abc.xyz/investor/other/google-code-of-conduct) ![google](../google/icon.png) kodeksie postępowania mniej więcej na przełomie kwietnia i maja. Pozostało ono tylko na samym końcu dokumentu: *I pamiętaj: nie bądź zły, a jeżeli zauważysz coś niewłaściwego, protestuj.*

Nowa wersja kodeksu brzmi… no cóż, bardzo *firmowo*. Zawiera ogólne wytyczne na temat etyki postępowania i brzmi podobnie, jak dokumenty umieszczane przez inne wielkie korporacje. Google ponad dekadę temu dawał do zrozumienia, że nie chce być takim jak one. Wygląda na to, że był to wyłącznie przejaw naiwności.

### Don’t be evil? To mydlenie oczu trwa dłużej, niż niektórym się wydaje.

#### Zakusy monopolistyczne.

Google już od dawna nie różni się od innych wielkich korporacyjnych molochów jeśli chodzi o wybiórcze podejście do etyki. Firma [została już ukarana](https://spidersweb.pl/2017/06/google-kontra-komisja-europejska) ![google](../google/icon.png) ![amazon](../amazon/icon.png) przez władze Unii Europejskiej karą 2,7 mld euro za nadużywanie swojej monopolistycznej pozycji na terenie UE i pozycjonowanie w Wyszukiwarce Google swoich usług wyżej od konkurencyjnych, świadomie utrudniając ekspozycję usług konkurencyjnych.

#### O Google trzeba pisać dobrze. Zawsze.

Pamiętacie jeszcze o Google+? Kashmir Hill, dziennikarka Forbesa, uczestniczyła w spotkaniu jej redakcji z firmą Google. Ta powiedziała jej przedstawicielom wprost, że jeżeli na Forbesie nie pojawią się przyciski *+1* przy artykułach – taki odpowiednik facebookowego *lajka* – to Forbes będzie znacznie gorzej pozycjonowany w wyszukiwarce. Hill odebrała to jako szantaż i używając tej narracji napisała [artykuł opisujący spotkanie](https://gizmodo.com/yes-google-uses-its-power-to-quash-ideas-it-doesn-t-like-1798646437) ![google](../google/icon.png) ![amazon](../amazon/icon.png). Artykuł, który Google później starannie depozycjonował w wynikach wyszukiwania.

#### Kombinowanie z podatkami.

Google jest znany ze swojej kreatywnej księgowości, choć nie zawsze jej to uchodzi na sucho. Włoskiemu fiskusowi w tym roku udało się odzyskać 303 mln euro podatku, który Google sobie przeksięgował do Irlandii. Trwają podobne postępowania we Francji i Hiszpanii. W 2016 r. brytyjskiemu fiskusowi udało się wyszarpać od Google’a 130 mln funtów, które również przeksięgowano na Irlandię. Co jeszcze zabawniejsze, w samej Irlandii Google zadeklarował w tamtym roku 22 mld euro przychodu, od czego zapłacił… 42 mln euro podatku.

#### Wspieranie fundacji jest fajne. No chyba, że nam przeszkadzają, wtedy trzeba je zniszczyć.

Google chętnie wspiera finansowo różne inicjatywy rozwojowe i think-tanki. Przez długi czas łożył pieniądze na organizację NAF (New America Foundation). Jednym z jej projektów stało się Open Markets, którego celem była walka ze skutkami monopoli w biznesie. W 2017 r. lider Open Markets skrytykował Google’a i pochwalił działania Unii Europejskiej wymierzone w firmę. Google w efekcie [doprowadził do jego zwolnienia](https://theguardian.com/technology/2017/aug/30/new-america-foundation-google-funding-firings) ![google](../google/icon.png) ![facebook](../facebook/icon.png) ![amazon](../amazon/icon.png) ![twitter](../twitter/icon.png) i rozwiązania inicjatywy Open Markets. Google nie jest właścicielem NAF, tak żeby była jasność.

#### Reklamy nielegalnych produktów? Dopóki klient płaci…

W latach 2003-2011 Google nie robił problemów kanadyjskim firmom farmaceutycznym, które kupowały u niego kampanie reklamowe targetowane w amerykańskich konsumentów. Wiedząc, że handel medykamentami niezatwierdzonymi przez amerykańską Agencję Żywności i Leków jest nielegalny. Niezbadane pigułki które licho-wie-co robią i jak działają? A niech się ci internauci trują – grunt, że kasa się zgadza. Google [musiał zapłacić 500 mln dol kary](https://nakedsecurity.sophos.com/real-canadian-pharmacies-cost-google-500-million-dollars) ![google](../google/icon.png) ![twitter](../twitter/icon.png) ![linkedin](../linkedin/icon.png) za te praktyki.

#### Wybiórcza cenzura.

Klient sieci społecznościowej Gab został wywalony ze Sklepu Play za tak zwaną mowę nienawiści. Nie bez powodu – popularność na Gabie zyskiwały profile amerykańskich skrajnych prawicowców, o otwarcie nazistowskich poglądach. Nikt nie powinien mieć z tym problemu, gdyby nie fakt, że Twitter i YouTube – sieci społecznościowe wypełnione mową nienawiści po brzegi – nadal są dostępne w Sklepie Play.

#### Bezprawne podsłuchiwanie obywateli bez ich wiedzy i zgody.

Gdybym sam miał wskazać kiedy przestałem wierzyć w motto Google’a, bez wahania wspomniałbym o skandalu z 2010 r. Wtedy to Google ciężko pracował nad rozwojem usługi Street View, wysyłając na cały świat flotę samochodów fotografujących w zasadzie wszystko. Samochody te zbierały również informacje o geograficznym położeniu punktów dostępowych Wi-Fi. Co samo w sobie nie jest jeszcze niczym złym. Jak się jednak [później okazało](https://nakedsecurity.sophos.com/google-in-trouble-for-streetview-all-over-again-this-time-in-brazil) ![google](../google/icon.png) ![twitter](../twitter/icon.png) ![linkedin](../linkedin/icon.png), pojazdy Google’a zapisywały również transmisje danych w sieciach bezprzewodowych, zbierając gigantyczną ilość danych o nas wszystkich. Firma otwarcie kłamała, twierdząc, że nie robi niczego takiego i odmawiała współpracy z lokalnymi urzędami. Dopiero po serii kar finansowych zmieniła zdanie i się przyznała do procederu. Z czasem wręcz pogubiła się w swoich kłamstwach, deklarując że dane zostały zniszczone, po czym po jakimś czasie przyznając, że… jednak nie.
