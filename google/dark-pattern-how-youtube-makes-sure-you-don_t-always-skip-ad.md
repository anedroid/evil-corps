## Dark Pattern: How Youtube Makes Sure You Don’t Always “Skip Ad”

źródło: <https://scribe.citizen4.eu/dark-pattern-how-youtube-makes-sure-you-dont-always-skip-ad-e6ab2117e3b5> (oryginalny post został opublikowany w serwisie Medium. Scribe pozwala na czytanie postów z serwisu Medium bez narażania prywatności użytkowników)

użytkownik: *Allen*

opublikowano: 2022.05.02

---

We all know what design dark pattern is, click baits, misleading visuals, hidden fees, we see them every day. However, YouTube’s dark pattern is so dark that most users don’t even notice it.

!["skip ads" button](images/youtube-ads/skip-ads.png)

### The “Skip Ad” Button: The Devil You Never Expected

YouTube get paid by the advertisers when they insert video ads. And they get paid even more when they let the advertisers insert *longer* video ads. On the other hand, YouTube needs to make sure their users are not too bothered by the long ads, so they created the “Skip Ad” button. However, the advertisers don’t have to pay YouTube when their ads are skipped too soon ([via](https://influencermarketinghub.com/how-much-do-youtube-ads-cost) ![cloudflare](../cloudflare/icon.png) ![google](../google/icon.png) ![facebook](../facebook/icon.png) ![linkedin](../linkedin/icon.png)). This means YouTube can’t just let everyone click on the “Skip Ad” button all the time. Furthermore, if skipping ads was too easy, it would hurt the sales of YouTube’s ad-free subscription as well. So while YouTube offers the “Skip Ad” button, **YouTube ultimately wants the users to stay away from clicking the “Skip Ad” button**.

### How the Video Ads Could Have Been Implemented

So we know there are two types of video ads: the non-skippable ads and the skippable ads. In theory, there would naturally only be two design variations:

#### **#1 The Non-skippable: ad plays for a few seconds and ends**

![](images/youtube-ads/non-skippable-ad.png)

#### **#2 The Skippable: ad plays for a few seconds and a “Skip Ad” button appears**

![](images/youtube-ads/skippable-ad.png)

Simple right? YouTube could arrange these ads however they want. Spread them out, put them one after another. It can be whatever, but it would always be one of these two design variations. Easy. *Predictable*.

Now compare these two ads with how YouTube actually does it.

## How Youtube Implemented the Video Ads IRL

From YouTube’s perspective, they need to make sure their users don’t always click on the “Skip Ad” button due to the reasons mentioned. How? **Make it as hard as possible to predict when the “Skip Ad” button would appear**. This is why YouTube came up with *at least* 5 variations.

#### **#1 Non-skippable Ad:** “Video will play after ad(s)”

![](images/youtube-ads/non-skippable-ad-1.png)

#### **#2 Non-skippable Ad:** “Ad will end in X”

![](images/youtube-ads/non-skippable-ad-2.png)

#### **#3 Non-Skippable Ad:** A countdown only

![](images/youtube-ads/non-skippable-ad-3.png)

#### **#4 Skippable Ad:** “You can skip to video in X”, then a “Skip Ad” button appears

![](images/youtube-ads/skippable-ad-1.png)

#### *#5 Skippable Ad:** A countdown only, then a “Skip Ad” button appears

![](images/youtube-ads/skippable-ad-2.png)

> **By introducing so many versions of design, it forbids a pattern to be established and recognized by the users. Essentially, the users look at the design, get confused about if there is going to be a “Skip Ad” button or not, get tired of trying to figure it out, and finally pay less attention to it which increases the chance of them ending up watching ads that could have been skipped.**

Here are some design details. Not only YouTube came up with various designs for something that could have been much simpler, YouTube created a secondary counter at the top left corner (sometimes bottom left depends on your device), conveniently located at the other side of where the “Skip Ad” button could be. This draws users’ attention away from monitoring the bottom right corner. Furthermore, sometimes the secondary counter is redundant and sometimes it’s not. Sometimes you get to skip shorter ads and sometimes you don’t. Sometimes you get a count down that would result in a “Skip Ad” button and sometimes you don’t. YouTube is doing everything it can to break any patterns you could potentially find and memorize.

Bonus points, see the yellow “Ad” label in the screenshots? It is written in white on yellow, likely another dark pattern where YouTube picked the two colors that have low contrast between them to decrease readability. Also, when a countdown tells you there are X seconds left, there are actually about X+0.5 seconds left ([via](https://teddit.net/r/ProgrammerHumor/comments/8dexhv/youtube_tries_to_sneak_in_an_extra_half_second_of)). This is likely why YouTube avoided the word *seconds*.

![intuitive design vs youtube's design](images/youtube-ads/youtube-design.png)

Now that YouTube’s secret formula is revealed here, you may think that it is not that hard to figure out this encrypted pattern. However, in real life, not only the users are working with limited time, a video ad would also be playing in the background when all this is happening.

*Note: YouTube’s variation #4 seems to be currently missing which could indicate that YouTube has removed it because “You can skip to video in X” likely encouraged too many skips and decreased advertsers’ willingness to buy ads.*

### The Ethical Dilemma

In theory, by displaying more or fewer of some of these design variations, YouTube gets to fine-tune the exact percentage of skippable ads being skipped vs not which has a direct impact on revenue. This is an incredible asset to YouTube but it doesn’t come free. YouTube is essentially trading usability for revenue. On the other hand, is this acceptable as it does no big harm to the users? Like most ethical dilemmas, there may not be a clear right or wrong answer but what are your thoughts?

If you don’t agree with what YouTube is doing, share the article so more people can be aware of YouTube’s dark pattern.
