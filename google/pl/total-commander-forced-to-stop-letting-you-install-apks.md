## Total Commander zmuszony przestać pozwalać na instalację APK-ów

**Developer podejmuje działanie w związku ze skargą Google dotyczącą polityki dystrybucji aplikacji w Google Play**

[Oryginalny artykuł](../total-commander-forced-to-stop-letting-you-install-apks.md) został napisany w języku angielskim.

źródło: <https://androidpolice.com/total-commander-apk-installation-block> ![google](../../google/icon.png)

użytkownik: *Will Sattelberg*

opublikowano: 2022.05.17

tłumaczenie: *Anedroid* | Jeżeli znalazłeś błąd, daj znać na Mastodonie lub wyślij pull request.

---

Jedną z najlepszych funkcji Androida, który zostawia swoją konkurencję na rynku systemów mobilnych w tyle jest możliwość instalacji aplikacji spoza Sklepu Google Play. To właśnie dzięki instalacji APK-ów możesz wciąż grać w Fortnite – nawet wtedy, gdy pomiędzy Epic i Google trwa spór prawny. Dzięki temu możesz również skrócić okres oczekiwania na automatyczne aktualizacje ulubionych aplikacji zawierające nowe funkcje i ulepszenia. Niestety jeden z najbardziej znanych i zaufanych menedżerów plików na system Android właśnie usunął możliwość instalowania plików APK, gdy developer otrzymał od Google ostrzeżenie, że jego aplikacja zostanie zdjęta.

Total Commander jest z nami od lat '90, a ponad dekadę temu jego wsparcie rozszerzyło się na Androida wkrótce po powstaniu tej platformy. Ta aplikacja ma ponad 10 milionów pobrań w Sklepie Google Play, i nadal wspiera bardzo stare wersje systemu począwszy od Androida 2.2. W najnowszej aktualizacji developer Christian Ghisler usunął z aplikacji możliwość instalacji plików APK, obwiniając za to politykę Google Play w liście zmian w tej wersji. Niezły zwrot akcji i przy okazji zły omen dla innych mobilnych menedżerów plików.

W [poście na forum](https://ghisler.ch/board/viewtopic.php?t=76643) Ghisler rzuca nieco więcej światła na całą sprawę, informując, że Google wysłał mu ostrzeżenie o usunięciu aplikacji z Google Play w przeciągu tygodnia, chyba że problem zostanie rozwiązany. Automatyczna odpowiedź firmy mówi o [naruszeniu "Polityki urządzeń i sieci"](https://support.google.com/googleplay/android-developer/answer/9888379?hl=pl) ![google](../../google/icon.png), w szczególności odnosząc się do tych dwóch sekcji:

> Aplikacja rozpowszechniana w Google Play nie może samodzielnie się modyfikować, zastępować ani aktualizować przy użyciu metody innej niż mechanizm aktualizacji Google Play. Aplikacja nie może też pobierać kodu wykonywalnego (np. plików .dex, .jar i .so) z innego źródła niż Google Play. Nie dotyczy to kodu, który jest uruchamiany na maszynie wirtualnej czy w interpreterze z pośrednim dostępem do interfejsów API Androida (na przykład JavaScript w komponencie WebView lub przeglądarce).
> 
> Aplikacje lub kod innej firmy (np. SDK) z interpretowanymi językami (JavaScript, Python, Lua itp.) ładowanymi podczas działania (np. niespakowanymi z aplikacją) nie mogą dopuszczać do potencjalnych naruszeń zasad Google Play.

W oparciu o te zasady, automatyczny system moderacji Google Play mógł zinterpretować zachowanie aplikacji jako próbę samoaktualizacji z pominięciem mechanizmów Google Play. Ghisler napisał, że podejmował próbę zablokowania instalowania przez Total Comander swojego własnego pliku APK, niestety automatyczny system skanując jego aplikację w poszukiwaniu potencjalnych naruszeń nie wyłapał zmiany. Po ponownym wysłaniu otrzymał tylko następującą odpowiedź:

> Jak już wspomnieliśmy, twoja aplikacja (wersje APK 1031, 1032, 1033, 1034, 1035 i 1036) powoduje, że użytkownicy pobierają i instalują aplikacje z niezaufanych źródeł spoza Google Play.

Wtedy Ghisler podjął decyzję o całkowitym usunięciu opcji instalowania plików APK, z obawy przed utratą dostępu do konta po otrzymaniu trzeciego ostrzeżenia – co spotkało już innych developerów w podobnych okolicznościach.

Możliwe, że to wydarzenie w przyszłości będzie miało duży wpływ na los menedżerów plików i przeglądarek internetowych w Sklepie Google Play, chociaż użyty tutaj język – nie wspominając o [niepochlebnym dla reputacji Google fałszywym blokowaniu aplikacji](https://androidpolice.com/play-store-leafsnap-simple-keyboard-takedown) ![google](../../google/icon.png) – wydaje się wskazywać na coś mniej podstępnego. Bazując na informacjach dostarczonych przez Ghislera, wygląda na to, że Google uważa, że Total Commander aktualizuje się od wewnątrz łącząc się z określonymi witrynami hostującymi pliki APK, albo stosuje własną metodę instalacji przed przekierowaniem użytkownika do domyślnego instalatora systemu Android. W każdym razie ta sytuacja wymaga dokładniejszego wyjaśnienia ze strony firmy. Google powinien albo sprecyzować, co Total Commander robi źle, a czego nie robią inne menedżery plików, albo pozwolić na przywrócenie aplikacji w Google Play do jej poprzedniego stanu.

Wysłaliśmy do Google prośbę o komentarz, po otrzymaniu odpowiedzi zaktualizujemy artykuł. W tym czasie lepiej zablokuj aktualizacje Total Commandera jeżeli używasz tej aplikacji do instalacji APK-ów.

![Logo Sklepu Google Play](../images/play-store-header.png)
