## Twoje konto Google zostało zablokowane z powodu kodu w Pythonie!

[Oryginalny artykuł](../your-google-account-has-been-disabled-because-of-a-python-code.md) został napisany w języku angielskim.

źródło: <https://teddit.net/r/GMail/comments/ukaauk/your_google_account_has_been_disabled_because_of> (oryginalny post został opublikowany w serwisie Reddit. Strona teddit.net pozwala na czytanie wątków Reddit bez narażania prywatności użytkowników)

użytkownik: *m9nasr*

opublikowano: 2022.05.07

tłumaczenie: *Anedroid* | Jeżeli znalazłeś błąd, daj znać na Mastodonie lub wyślij pull request.

---

Pięć dni temu obudziłem się i odkryłem, że moje konto Google zostało zablokowane z powodu "**szkodliwej treści**". Zszokowało mnie to. Moje konto było dla mnie wszystkim: przechowywałem na nim swoje zdjęcia odkąd byłem dzieckiem (łącznie **zdjęcia z 23 lat**), zdjęcia mojej siostrzenicy począwszy od zdjęć przed narodzinami, moje najważniejsze pliki, WSZYSTKIE hasła, przypomnienia i konta do których logowałem się przez Google. Wszystko przepadło!

Nie tylko to: moje konto Google Play razem z moimi aplikacjami, mój Google Extension Developer z moimi rozszerzeniami, mój Google AdMob z wszystkimi moimi nieodebranymi przychodami, Firebase, Google Analytics i mój Google Search Console...

Napisałem do Google emaila z prośbą o wyjaśnienie i otrzymałem standardową odpowiedź (złamałeś warunki korzystania z usługi). Zaczęłem więc zbierać informacje o tym, co robiłem w ciągu ostatnich 48 godzin, zanim oni arbitralnie zamknęli mi konto. Wysłałem wtedy skrypt w Pythonie (napisany przeze mnie) do mojego Dysku Google aby szybko go przesłać do mojego RDP. Skrypt tylko pobierał kopię zapasową danych z mojej aplikacji! Google uznał go za szkodliwy! Jak mogło do tego dojść! TO BYŁ ZWYKŁY, NORMALNY KOD!

Jestem rozczarowany i przygnębiony. Pamiętam, że zawsze polecałem znajomym zapisywanie zdjęć w chmurze w aplikacji Zdjęcia Google. Jak bardzo się myliłem...
